 console.log( fetch(`https://jsonplaceholder.typicode.com/posts`) )

 
 fetch(`https://jsonplaceholder.typicode.com/posts`)
 .then( data => data.json() )
 .then( data => {

     console.log(data)

     data.forEach(element => {
         console.log(element)
     })
 })



 fetch(`https://jsonplaceholder.typicode.com/posts/1`)
 .then(response => response.json())
 .then(response => {
     console.log(response)
 })




fetch(`https://jsonplaceholder.typicode.com/posts`, {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "New Post",
        body: "Hello World",
        userId: 1
    })
})
.then( response => response.json() )
.then( response => {
    console.log(response)
})


PUT METHOD to update a specific document
fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated Post",
        body: "Hello updated post!",
        userId: 1
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})



PATCH METHOD applies update to a particular document's field/s
fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Corrected Post thru patch method"
    })
})
.then( data => data.json())
.then(data => {
    console.log(data)
})



document
fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
    method: "DELETE"
})



    filtering data by sending a query via the URL
    single query:    endpoint?parameter=value
    multiple query:  endpoint?parameterA=valueA&parameterB=valueB
fetch(`https://jsonplaceholder.typicode.com/posts?userId=1&id=1`)
.then(data => data.json())
.then(data => {
    console.log(data)
})
